let students = [];

function addStudent(student){
	let addStudent = students.push(student);
	console.log(student + " was added to the student's list.");
};

function countStudents(){
	console.log("There are a total of " + students.length + " students enrolled.");
}

function printStudents(){
	students.sort();
	students.forEach(function(student){
		console.log(student.toLowerCase());
	})
}

function findStudents(studentName){
	let filteredNames = students.filter(function(student){
		return student.includes(studentName.toLowerCase());
	})
	if (filteredNames.length >= 2){
		console.log(filteredNames.toString() + " are enrollees.");
	}
	else if (filteredNames.length === 1){
		console.log(studentName + " is an enrollee.");
	}
	else
		console.log('No student found with the name ' + studentName);

}

// function findStudent()
	